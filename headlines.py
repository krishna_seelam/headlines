import feedparser
from cachetools import cached, TTLCache
from flask import Flask, render_template,request,redirect,url_for
from forms  import PublicationForm

from utils import DEFAULT_RSS ,RSS_FEEDS,get_elapsed


app = Flask(__name__)
app.config['SECRET_KEY'] = 'naja'
cache = TTLCache(maxsize=100, ttl=300)

@cached(cache)  
def load_from_rss():
    headlines={}
    for feed_source in RSS_FEEDS:
         feed  = feedparser.parse(RSS_FEEDS.get(feed_source,DEFAULT_RSS))
         for entry in feed['entries']:
             entry['published_ago'] = get_elapsed(entry.get('published',None))
         headlines[feed_source] = feed['entries'][0:5]
    return headlines 

@app.route('/')
def index():
    form  = PublicationForm(request.form)
    headlines =load_from_rss()
    return render_template('index.html', title='Home',form=form, headlines=headlines), 200


@app.route('/get_news',methods=['GET','POST'])
def get_news():
    form  = PublicationForm(request.form)
    headlines =load_from_rss()
    
    if request.method == 'GET':
        return render_template('index.html',title='Home',form=form,headlines=headlines),200
    
    if form.validate_on_submit():
        publication = form.publication.data.lower()
        publication_feed = headlines.get(publication,None)
        if publication_feed:
            return render_template('index.html', title='Home', form=form,headlines={publication:publication_feed}), 200
    return  redirect(url_for('index'))


if __name__ == '__main__':
    app.run(port=5001, debug=False)
