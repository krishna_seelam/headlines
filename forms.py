from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField,SubmitField
from wtforms.validators import DataRequired

class PublicationForm(FlaskForm):
    publication = StringField('Please enter news publication', validators=[DataRequired()])
    login = SubmitField('get_news')