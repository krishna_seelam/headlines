from datetime import datetime

DEFAULT_RSS='http://feeds.bbci.co.uk/news/rss.xml'
RSS_FEEDS = {'bbc': 'http://feeds.bbci.co.uk/news/rss.xml',
             'cnn': 'http://rss.cnn.com/rss/edition.rss',
             'fox': 'http://feeds.foxnews.com/foxnews/latest',
             'iol': 'http://www.iol.co.za/cmlink/1.640'}



def get_elapsed(strdate):
    try:
        published_dt = datetime.strptime(strdate,'%a, %d %b %Y %H:%M:%S %Z')
        diff  = (datetime.utcnow() - published_dt).seconds // 3600
        return str(diff) + ' hours ago'
    except Exception:
        return ' '